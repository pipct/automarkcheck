class EnvConf
  def initialize(hash)
    hash.each do |k,v|
      if !ENV.key? v
        raise "Configuration environment variable '#{v}' not set"
      end
      self.class.send(:define_method, k, proc{ENV[v]})
    end
  end
end

module JSONFiles
  # you might call this ridiculous, but hey, it's ruby:
  # when in rome, stay in rome :)
  #
  # actually, when in this particular rome i should probably be using activerecord...

  @@_json_file_defaults = {}

  def new(dir, *args)
    @@_json_file_defaults.each do |name, default|
      if !File.exist?(File.join(dir, name.to_s))
        open(File.join(dir, name.to_s), 'w') do |file|
          file.write(JSON.generate(default))
        end
      end
    end

    inst = super(*args)
    inst.instance_variable_set(:@_json_file_dir, dir)
    inst
  end

  def json_file(name, default)
    # if !class_variable_defined? :@@_json_file_defaults
    #   @@_json_file_defaults = {}
    # end
    @@_json_file_defaults[name] = default

    define_method("#{name}") do
      open(File.join(@_json_file_dir, name.to_s), 'r') do |file|
        JSON.parse file.read
      end
    end

    define_method("#{name}=") do |val|
      open(File.join(@_json_file_dir, name.to_s), 'w') do |file|
        file.write(JSON.generate val)
      end
      val # i think this is how setters should work?
    end
  end
end
