#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'bundler/setup'

require 'set'
require 'json'
require 'socket'
require 'mechanize'
require 'faraday'

require_relative 'shared'

def url(path)
  'https://my.vuw.ac.nz' + path
end

class AutoMarkChecker
  def initialize
    @conf = EnvConf.new(
      # Hopefully given to you by J̄a̹͖͓̟ͦ̐̐̄mỉ́ͬ́̉̍è̋̎̃ͯͯ͆
      :token => 'AUTOMARKCHECK_AGENT_TOKEN',

      # I hope you trust this code...
      :user => 'VUW_ITS_USER',
      :pass => 'VUW_ITS_PASS',
    )

    @a = Mechanize.new do |agent|
      agent.user_agent_alias = 'Mac Safari' # 👀
    end

    @mutex = Mutex.new

    @startup = Time.now
  end

  def log_in
    @mutex.synchronize {
      @a.cookie_jar.clear

      login_page = @a.get(url '/cp/home/displaylogin')
      uuid = /cplogin.uuid.value=['"]([^'"]+)['"]/.match(login_page.body)[1]
      login_page.form_with(name: 'cplogin') do |f|
        f.user = @conf.user
        f.add_field!('pass', @conf.pass)
        f.uuid = uuid
      end.submit

      # Sets important cookies
      @a.get(url '/cps/welcome/loginok.html')
      @a.get(url '/cp/home/next')
    }
  end

  def fetch_marks
    self.log_in
    page = @mutex.synchronize {
      # For Diony, it's:
      #@a.get(url '/tag.c56f3aaeaf27f1c8.render.userLayoutRootNode.uP?uP_root=u12l1n642')
      # , and the normal one is blank

      # For Nick and Jamie, it's:
      @a.get(url '/tag.e346fb87a7e9ef60.render.userLayoutRootNode.uP?uP_root=u12l1n642')

      # weird...
      # TODO look into this :P
    }

    if !page.respond_to?(:css)
      puts "Warning: network error or something? Fetching marks failed."
     return []
    end

    return page
      .css("table.datadisplaytable tr")
      .map { |x| x.css("td").map(&:text) }
      .select { |tds| tds.length == 5 }
  end

  def run
    loop do
      data = begin
        {
          :courses => self.fetch_marks.map do |row|
            # row[4] is your grade -- for privacy, we just send whether it exists or not
            ["#{row[1]}#{row[2]}", !row[4].strip.empty?]
          end.to_h
        }
      rescue StandardError => e
        {
          :error => e.inspect
        }
      end
      data[:token] = @conf.token

      timediff = Time.now - @startup
      data[:uptime] = "#{timediff.to_i / 86400} days, #{Time.at(timediff).utc.strftime("%T")}"

      data[:hostname] = Socket.gethostname
      p data

      begin
        res = Faraday.post 'http://automarkcheck.kwiius.com:4567/yeet' do |req|
          req.headers['Content-Type'] = 'application/json'
          req.body = JSON.generate(data)
        end

        if !res.success?
          puts "Failed to report to bot: #{res.status}"
        end
      rescue StandardError => e
        puts "Error while reporting to bot: #{e.inspect}"
      end

      # Random time to avoid thundering herd on ITS's poor webserver <3
      sleep rand(50..70)
      # Subsecond as well
      sleep rand
    end
  end
end

AutoMarkChecker.new.run
