# AutoMarkCheck

## Getting Started (running the agent)
* Ensure `ruby` is installed, along with `build-essential patch ruby-dev zlib1g-dev liblzma-dev`, then [`sudo`?] `gem install bundler`
* In this directory, `bundle install --deployment --without bot`
* Set `AUTOMARKCHECK_AGENT_TOKEN`, `VUW_ITS_USER`, and `VUW_ITS_PASS` in the environment
* Run `agent.rb`
